import math

def computeIntegral(lower,upper,N):

    dx = (upper-lower)/N
    integral = 0

    for i in range(N):
        xip12 = dx*(i + 0.5)
        dI = abs(math.sin(xip12))*dx
        integral += dI

    return integral