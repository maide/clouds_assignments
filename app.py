from numerical_integration import computeIntegral
from flask import Flask

app = Flask(__name__)

@app.route('/integral/<lower>/<upper>')
def integral(lower,upper):
    integrals = 'The integrals are ' + '\n'
    for i in range(1,8):
        integrals += str(computeIntegral(float(lower),float(upper),10**i))+'\n'
    return integrals